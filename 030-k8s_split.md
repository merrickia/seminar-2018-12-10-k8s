@title[Kubernetes]

@snap[south-west template-note text-gray]
What Can Kubernetes Do...?
@snapend

@snap[west split-screen-heading span-30]
![whalescluster](img/k8s-whale-cluster_512.png)
@snapend

@snap[east text-white span-65]
@ol[split-screen-list](false)
- Runs containerized applications across a cluster of machines
- Manages applications - scaling (horizontal, vertical), rolling updates
- Adds resilience to applications/containers - restarts failed workloads
- Connects applications - internal DNS, service discovery
- Manages persistent volumes
@olend
@snapend



